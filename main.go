package ElectrumApi

import (
	"io/ioutil"
	"net/http"
	"strings"
)

type El struct {
	UserName string
	Password string
	URL      string
	Port     string
}

func Init(user, passwd, url, port string) *El {
	return &El{
		UserName: user,
		Password: passwd,
		URL:      url,
		Port:     port,
	}
}

func (e *El) Api(method, params string) (string, error) {
	var bodyString string

	body := strings.NewReader(`{"id": "curltext", "method": "` + method + `", "params": ` + params + `}`)
	req, err := http.NewRequest("POST", "http://"+e.UserName+":"+e.Password+"@"+e.URL+":"+e.Port, body)
	if err != nil {
		return bodyString, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return bodyString, err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return bodyString, err
	}

	bodyString = string(bodyBytes)
	return bodyString, nil
}

/*
	curl --data-binary '{"id":"curltext","method":"getbalance","params":[]}' http://user_test:test123@127.0.0.1:7778
	getbalance {"result": {"confirmed": "0.03518357"}, "id": "curltext", "error": null}
*/

// request
/*	var method, params string
	// method = "getbalance"
	// method = "history"

	method = "listaddresses"
	params = `{"funded":true}`

	out, err := el.Api(method, params)
	if err != nil {
		log.Println("err:", err)
		return
	}

	result := gjson.Get(out, "result")
	log.Printf("%s\n", result)*/
